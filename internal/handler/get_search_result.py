import json
import sys

import tornado.ioloop
import tornado.web
import traceback

from collections import Counter

from internal.bucket.bucket import d_search_asset
from fuzzywuzzy import fuzz
from fuzzywuzzy import process


class GetSearchResultHandler(tornado.web.RequestHandler):
    def initialize(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')

        self.set_status(500)

    def __search_kb(self, query, l_choice):
        return process.extract(query, l_choice, limit=10, scorer=fuzz.partial_ratio)

    def __get_fuzzy_score(self, query, sentence):
        return fuzz.partial_ratio(query, sentence)

    def __get_chunk_relevant(self, query, content):
        l_sent = content.split('\n')
        best_idx   = -1
        best_score = -1
        for idx, sent in enumerate(l_sent):
            if sent.strip() == '':
                continue
            if self.__get_fuzzy_score(query, sent) > best_score:
                best_score = self.__get_fuzzy_score(query, sent)
                best_idx = idx
        
        if best_idx + 1 >= len(l_sent):
            right_content = ''
        else:
            right_content = ' '.join(l_sent[best_idx:])

        relevant_content = l_sent[idx] + right_content
        return relevant_content.strip().replace('\n', '.'), best_score

    def __get_search_result(self, query, lang_code):
        l_result = []
        c_similar_url = Counter()

        l_search_url     = d_search_asset[lang_code]['search_url']
        l_search_title   = d_search_asset[lang_code]['search_title']['list_text']
        l_search_content = d_search_asset[lang_code]['search_content']['list_text']

        d_search_title   = d_search_asset[lang_code]['search_title']['mapping']
        d_search_content = d_search_asset[lang_code]['search_content']['mapping']

        for idx in range(len(l_search_title)):
            cur_url = l_search_url[idx]
            title   = l_search_title[idx]
            content = l_search_content[idx]
            
            title_score   = self.__get_fuzzy_score(query, title)
            content_score = self.__get_fuzzy_score(query, content)
            total_score = title_score + content_score
            c_similar_url[cur_url] = total_score

        for similar_url, _ in c_similar_url.most_common(5):
            relevant_content, _ = self.__get_chunk_relevant(query, d_search_content[similar_url])
            l_result.append({
                'url': similar_url,
                'title': d_search_title[similar_url],
                'relevant_content': relevant_content[:500] + '...'
            })
            
        return l_result

    def get(self):
        q = self.get_argument('q').strip().lower()
        lang_code = self.get_argument('lang_code', 'vi').strip().lower()
        d_result = {
            'text': q,
            'lang_code': lang_code,
            'results': [],
        }

        try:
            if q != '':
                l_result = self.__get_search_result(q, lang_code)
                d_result['results'] = l_result

            self.set_status(200)

        except Exception:
            traceback.print_exc(file=sys.stdout)

        self.set_header("Content-Type", 'application/json')
        self.write(json.dumps(d_result, ensure_ascii=False))
        self.finish()
