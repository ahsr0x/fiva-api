import json
import sys
import requests
import os
import threading

import tornado.ioloop
import tornado.web
import traceback

from bson.objectid import ObjectId
from internal.bucket.bucket import question_category_col, question_col, entity_col


RASA_API = os.environ.get('RASA_API')

class AddQuestionCategoryHandler(tornado.web.RequestHandler):
    def initialize(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')

        self.set_status(500)

    def post(self):
        d_request = tornado.escape.json_decode(self.request.body)

        intent_name = d_request['intent_name'].strip()
        response = d_request['response'].strip()

        d_result = {
            'inserted_id': None,
            'success': False
        }

        try:
            if intent_name != '' and response != '':
                question_category_col.update({'intent_name': intent_name}, {
                    'intent_name': intent_name,
                    'response': response
                }, upsert=True)
                
                inserted_question_category = question_category_col.find_one({'intent_name': intent_name})
                d_result['inserted_id'] = str(inserted_question_category.get('_id'))
                d_result['success'] = True

            self.set_status(200)

        except Exception:
            traceback.print_exc(file=sys.stdout)

        self.set_header("Content-Type", 'application/json')
        self.write(json.dumps(d_result, ensure_ascii=False))
        self.finish()


class AddQuestionHandler(tornado.web.RequestHandler):
    def initialize(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')

        self.set_status(500)

    def post(self):
        d_request = tornado.escape.json_decode(self.request.body)

        question = d_request['question'].strip()
        question_category_id = d_request['question_category_id'].strip()

        d_result = {
            'inserted_id': None,
            'success': False
        }

        try:
            if question != '':
                question_col.update({'question': question}, {
                    'question_category_id': ObjectId(question_category_id),
                    'question': question
                }, upsert=True)

                inserted_question = question_col.find_one({'question': question})
                d_result['inserted_id'] = str(inserted_question.get('_id'))
                d_result['success'] = True

            self.set_status(200)

        except Exception:
            traceback.print_exc(file=sys.stdout)

        self.set_header("Content-Type", 'application/json')
        self.write(json.dumps(d_result, ensure_ascii=False))
        self.finish()


class AddEntityHandler(tornado.web.RequestHandler):
    def initialize(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')

        self.set_status(500)

    def post(self):
        d_request = tornado.escape.json_decode(self.request.body)

        entity = d_request['entity'].strip()

        d_result = {
            'inserted_id': None,
            'success': False
        }

        try:
            if entity != '':
                entity_col.update({'entity': entity}, {
                    'entity': entity
                }, upsert=True)

                inserted_entity = entity_col.find_one({'entity': entity})
                d_result['inserted_id'] = str(inserted_entity.get('_id'))
                d_result['success'] = True

            self.set_status(200)

        except Exception:
            traceback.print_exc(file=sys.stdout)

        self.set_header("Content-Type", 'application/json')
        self.write(json.dumps(d_result, ensure_ascii=False))
        self.finish()


class UpdateQuestionHandler(tornado.web.RequestHandler):
    def initialize(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')

        self.set_status(500)

    def post(self):
        d_request = tornado.escape.json_decode(self.request.body)

        question_id = d_request['question_id'].strip()
        question = d_request['question'].strip()

        d_result = {'success': False}
        try:
            if question != '':
                question_col.update({'_id': ObjectId(question_id)}, {
                    '$set': {
                        'question': question
                    }
                })

                d_result['success'] = True

            self.set_status(200)

        except Exception:
            traceback.print_exc(file=sys.stdout)

        self.set_header("Content-Type", 'application/json')
        self.write(json.dumps(d_result, ensure_ascii=False))
        self.finish()


class UpdateQuestionCategoryHandler(tornado.web.RequestHandler):
    def initialize(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')

        self.set_status(500)

    def post(self):
        d_request = tornado.escape.json_decode(self.request.body)

        question_category_id = d_request['question_category_id'].strip()
        intent_name = d_request['intent_name'].strip()
        response = d_request['response'].strip()

        d_result = {'success': False}
        try:
            if response != '':
                question_category_col.update({'_id': ObjectId(question_category_id)}, {
                    '$set': {
                        'response': response,
                        'intent_name': intent_name
                    }
                })

                d_result['success'] = True

            self.set_status(200)

        except Exception:
            traceback.print_exc(file=sys.stdout)

        self.set_header("Content-Type", 'application/json')
        self.write(json.dumps(d_result, ensure_ascii=False))
        self.finish()


class UpdateEntityHandler(tornado.web.RequestHandler):
    def initialize(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')

        self.set_status(500)

    def post(self):
        d_request = tornado.escape.json_decode(self.request.body)

        entity_id = d_request['entity_id'].strip()
        entity = d_request['entity'].strip()

        d_result = {'success': False}
        try:
            if entity != '':
                entity_col.update({'_id': ObjectId(entity_id)}, {
                    '$set': {
                        'entity': entity
                    }
                })

                d_result['success'] = True

            self.set_status(200)

        except Exception:
            traceback.print_exc(file=sys.stdout)

        self.set_header("Content-Type", 'application/json')
        self.write(json.dumps(d_result, ensure_ascii=False))
        self.finish()


class DeleteQuestionHandler(tornado.web.RequestHandler):
    def initialize(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')

        self.set_status(500)

    def post(self):
        d_request = tornado.escape.json_decode(self.request.body)

        question_id = d_request['question_id'].strip()

        d_result = {'success': False}
        try:
            question_col.delete_one({'_id': ObjectId(question_id)})
            d_result['success'] = True
            self.set_status(200)

        except Exception:
            traceback.print_exc(file=sys.stdout)

        self.set_header("Content-Type", 'application/json')
        self.write(json.dumps(d_result, ensure_ascii=False))
        self.finish()


class DeleteQuestionCategoryHandler(tornado.web.RequestHandler):
    def initialize(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')

        self.set_status(500)

    def post(self):
        d_request = tornado.escape.json_decode(self.request.body)

        question_category_id = d_request['question_category_id'].strip()

        d_result = {'success': False}
        try:
            
            question_category_col.delete_one({'_id': ObjectId(question_category_id)})
            d_result['success'] = True
            self.set_status(200)

        except Exception:
            traceback.print_exc(file=sys.stdout)

        self.set_header("Content-Type", 'application/json')
        self.write(json.dumps(d_result, ensure_ascii=False))
        self.finish()


class DeleteEntityHandler(tornado.web.RequestHandler):
    def initialize(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')

        self.set_status(500)

    def post(self):
        d_request = tornado.escape.json_decode(self.request.body)

        entity_id = d_request['entity_id'].strip()

        d_result = {'success': False}
        try:
            
            question_category_col.delete_one({'_id': ObjectId(entity_id)})
            d_result['success'] = True
            self.set_status(200)

        except Exception:
            traceback.print_exc(file=sys.stdout)

        self.set_header("Content-Type", 'application/json')
        self.write(json.dumps(d_result, ensure_ascii=False))
        self.finish()


class ListQuestionHandler(tornado.web.RequestHandler):
    def initialize(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')

        self.set_status(500)

    def get(self):
        d_result = []
        try:
            l_question_category_record = question_category_col.find()
            for question_category_record in l_question_category_record:
                question_category_id = question_category_record['_id']
                question_category_name = question_category_record['intent_name']
                response = question_category_record['response']

                cur_result = {
                    'question_category_id': str(question_category_id),
                    'intent_name': question_category_name,
                    'variation': [],
                    'response': response
                }
                
                l_question_record = question_col.find({'question_category_id': question_category_record['_id']})

                for question_record in l_question_record:
                    cur_result['variation'].append({
                        'question': question_record['question'],
                        'question_id': str(question_record['_id'])
                    })
                
                d_result.append(cur_result)

            self.set_status(200)

        except Exception:
            traceback.print_exc(file=sys.stdout)

        self.set_header("Content-Type", 'application/json')
        self.write(json.dumps(d_result, ensure_ascii=False))
        self.finish()


class ListEntityHandler(tornado.web.RequestHandler):
    def initialize(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')

        self.set_status(500)

    def post(self):
        self.get()

    def get(self):
        d_result = []
        try:
            l_entity_record = entity_col.find()
            for entity_record in l_entity_record:
                entity_id = entity_record['_id']
                entity = entity_record['entity']

                d_result.append({
                    'entity_id': str(entity_id),
                    'entity': entity
                })

            self.set_status(200)

        except Exception:
            traceback.print_exc(file=sys.stdout)

        self.set_header("Content-Type", 'application/json')
        self.write(json.dumps(d_result, ensure_ascii=False))
        self.finish()


class GetModelStatusHandler(tornado.web.RequestHandler):
    def initialize(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')

        self.set_status(500)


    def get(self):
        d_result = {
            'success': False,
            'is_training': False
        }

        try:
            url = '{}/status'.format(RASA_API)

            headers = {
              'Content-Type': 'application/x-yaml'
            }

            response = requests.request("GET", url, headers=headers)
            d_response = json.loads(response.text)
            d_result['success'] = True
            if d_response['num_active_training_jobs'] > 0:
                d_result['is_training'] = True
            else:
                d_result['is_training'] = False

        except Exception as e:
            traceback.print_exc(file=sys.stdout)

        self.set_status(200)

        self.set_header("Content-Type", 'application/json')
        self.write(json.dumps(d_result, ensure_ascii=False))
        self.finish()


class TriggerTrainingHandler(tornado.web.RequestHandler):
    def initialize(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')

        self.set_status(500)


    def get(self):
        def request_training():
            url = '{}/model/train?force_training=false'.format(RASA_API)

            response = requests.request("POST", url, headers=headers, data=payload)

        d_result = {
            "success": False
        }

        try:
            payload= """language: vi

pipeline:
  - name: "internal.response.response_fiva_classifier.ResponseFivaClassifier"
"""

            headers = {
              'Content-Type': 'application/x-yaml'
            }
            
            x = threading.Thread(target=request_training)
            x.start()

            d_result['success'] = True

            self.set_status(200)

        except Exception as e:
            traceback.print_exc(file=sys.stdout)

        self.set_header("Content-Type", 'application/json')
        self.write(json.dumps(d_result, ensure_ascii=False))
        self.finish()

