import json
import sys
import os
import requests
import ast

import tornado
import tornado.ioloop
import tornado.web
import traceback

RASA_API = os.environ.get('RASA_API') 


class ParseHandler(tornado.web.RequestHandler):
    def initialize(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS')

        self.set_status(500)

    def post(self):
        d_request = tornado.escape.json_decode(self.request.body)
        text = d_request['text'].strip().lower()
        d_result = {
            'entities': [],
            'related_result': [],
            'intents': [],
        }

        try:
            if text != '':
                payload = {'sender': 1, 'text': text}
                response = requests.post('{}/model/parse'.format(RASA_API), data=json.dumps(payload))
                d_response = json.loads(response.text)

                d_result['related_result'] = d_response['related_result']
                d_result['entities'] = d_response['entities']
                try:
                    d_response['intents'][0]['response'] = ast.literal_eval(d_response['intents'][0]['response'])
                    d_result['intents'].append({
                        'intent_name': d_response['intents'][0]['intent_name'],
                        'confidence': d_response['intents'][0]['confidence'],
                        'custom_response': d_response['intents'][0]['response']
                    })
                except:
                    d_result['intents'].append({
                        'intent_name': d_response['intents'][0]['intent_name'],
                        'confidence': d_response['intents'][0]['confidence'],
                        'response': d_response['intents'][0]['response']
                    })


            self.set_status(200)

        except Exception:
            traceback.print_exc(file=sys.stdout)

        self.set_header("Content-Type", 'application/json')
        self.write(json.dumps(d_result, ensure_ascii=False))
        self.finish()
