import os
import pymongo

from os import listdir
from os.path import isfile, join

l_file_name_vi = [f for f in listdir('./website_corpus/vi') if isfile(join('./website_corpus/vi', f))]
l_file_name_en = [f for f in listdir('./website_corpus/en') if isfile(join('./website_corpus/en', f))]

d_search_asset = {
    'vi': {
        'search_url': [],
        'search_title': {
            'list_text': [],
            'mapping': {},
        },
        'search_content': {
            'list_text': [],
            'mapping': {},
        }
    },
    'en': {
        'search_url': [],
        'search_title': {
            'list_text': [],
            'mapping': {},
        },
        'search_content': {
            'list_text': [],
            'mapping': {},
        }
    }
}

for file_name in l_file_name_vi:
    with open('website_corpus/vi/{}'.format(file_name), 'r', encoding='utf-8') as f:
        l_line = f.readlines()

    url     = l_line[0].strip()
    title   = l_line[1].strip()
    content = '\n'.join(l_line[2:]).strip()

    d_search_asset['vi']['search_content']['mapping'][url] = content
    d_search_asset['vi']['search_content']['list_text'].append(content)

    d_search_asset['vi']['search_title']['mapping'][url] = title
    d_search_asset['vi']['search_title']['list_text'].append(title)

    d_search_asset['vi']['search_url'].append(url)

for file_name in l_file_name_en:
    with open('website_corpus/en/{}'.format(file_name), 'r', encoding='utf-8') as f:
        l_line = f.readlines()

    url     = l_line[0].strip()
    title   = l_line[1].strip()
    content = '\n'.join(l_line[2:]).strip()

    d_search_asset['en']['search_content']['mapping'][url] = content
    d_search_asset['en']['search_content']['list_text'].append(content)

    d_search_asset['en']['search_title']['mapping'][url] = title
    d_search_asset['en']['search_title']['list_text'].append(title)

    d_search_asset['en']['search_url'].append(url)

mongodb_uri = os.environ.get('DB_URI') 
mongodb_client = pymongo.MongoClient(mongodb_uri, connect=True)
mongodb_database = mongodb_client[os.environ.get('DB_NAME')]

question_category_col = mongodb_database['QuestionCategory']
question_col = mongodb_database['Question']
entity_col = mongodb_database['Entity']

