import tornado.ioloop
import tornado.web
import argparse

from internal.handler.get_search_result import GetSearchResultHandler
from internal.handler.qbank import *
from internal.handler.parse import ParseHandler

def make_app():
    return tornado.web.Application([
        (r"/search", GetSearchResultHandler),

        (r"/add_question_category", AddQuestionCategoryHandler),
        (r"/add_question", AddQuestionHandler),
        (r"/add_entity", AddEntityHandler),

        (r"/update_question", UpdateQuestionHandler),
        (r"/update_question_category", UpdateQuestionCategoryHandler),
        (r"/update_entity", UpdateEntityHandler),

        (r"/delete_question", DeleteQuestionHandler),
        (r"/delete_question_category", DeleteQuestionCategoryHandler),
        (r"/delete_entity", DeleteEntityHandler),

        (r"/list_question", ListQuestionHandler),
        (r"/list_entity", ListEntityHandler),

        (r"/parse", ParseHandler),

        (r"/trigger_training", TriggerTrainingHandler),
        (r"/get_model_status", GetModelStatusHandler)
    ])


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Paraphrase generation app argument')
    parser.add_argument('--port', type=int, default=7000, help='API port')
    parser.add_argument('--n_workers', type=int, default=1, help='Number of subprocess')

    args = parser.parse_args()

    server = tornado.httpserver.HTTPServer(make_app())
    server.bind(args.port)
    server.start(args.n_workers)
    print("All Apis loaded")

    tornado.ioloop.IOLoop.current().start()
