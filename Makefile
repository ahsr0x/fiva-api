run:
	PYTHONIOENCODING=utf8 RASA_API=http://nlp.fiva.vn:5005 DB_NAME=fiva DB_URI=mongodb://myUserAdmin:verysecretpassword@103.143.142.151 python3 -m internal.app.app --port 7000

run-mobi:
	PYTHONIOENCODING=utf8 RASA_API=http://nlp.fiva.vn:5007 DB_NAME=mobi DB_URI=mongodb://myUserAdmin:verysecretpassword@103.143.142.151 python3 -m internal.app.app --port 7002

run-mobi-test:
	PYTHONIOENCODING=utf8 RASA_API=http://localhost:5007 DB_NAME=mobi DB_URI=mongodb://myUserAdmin:verysecretpassword@103.143.142.151 python3 -m internal.app.app --port 7002
