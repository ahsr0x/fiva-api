import requests
import json

from yaml import load

stream = open('/Users/ThoNguyen/Desktop/workspace/fiva/domain.yml', 'r')
data = load(stream)
print(data.keys())

with open('./script/test.txt', 'r+') as f:
    d_question = json.loads(f.read())

for utter_name in data['responses']:
    question_category_url = "http://localhost:7000/add_question_category"

    intent_name = utter_name.replace('utter_', '')
    response = data['responses'][utter_name][0]['text']

    payload={'intent_name': intent_name, 'response': response}

    response = requests.request("POST", question_category_url, data=payload)
    d_result = json.loads(response.text)
    
    question_category_id = d_result['inserted_id']

    for question in d_question[intent_name]:
        question_url = "http://localhost:7000/add_question"

        payload={'question_category_id': question_category_id, 'question': question}

        response = requests.request("POST", question_url, data=payload)
        
        print(response)

