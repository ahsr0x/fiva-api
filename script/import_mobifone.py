import requests
import json

from yaml import load

with open('./script/question.txt', 'r+') as f:
    l_question = f.readlines()

with open('./script/mobi_intent.txt', 'r+') as f:
    l_intent = f.readlines()

d_question = {}
for i, intent in enumerate(l_intent):
    intent = intent.strip()
    if intent not in d_question:
        d_question[intent] = []
    
    d_question[intent].append(l_question[i])

for intent in d_question:
    question_category_url = "http://localhost:7000/add_question_category"

    intent_name = intent
    response = intent

    payload=json.dumps({'intent_name': intent_name, 'response': response})
    print(payload)

    response = requests.request("POST", question_category_url, data=payload)
    print(response.text)
    d_result = json.loads(response.text)
    
    question_category_id = d_result['inserted_id']

    for question in d_question[intent_name]:
        question_url = "http://localhost:7000/add_question"

        payload=json.dumps({'question_category_id': question_category_id, 'question': question})

        response = requests.request("POST", question_url, data=payload)
        
        print(response)

