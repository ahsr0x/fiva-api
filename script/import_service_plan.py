import requests
import json


with open('./script/service_plan.txt', 'r+') as f:
    l_service_plan = f.readlines()

# print(len(l_service_plan))
for service_plan in l_service_plan:
    entity_url = "http://localhost:7000/add_entity"

    payload=json.dumps({'entity': service_plan.strip()})
    print(payload)

    response = requests.request("POST", entity_url, data=payload)
    print(response.text)
    d_result = json.loads(response.text)
    
    entity_id = d_result['inserted_id']
