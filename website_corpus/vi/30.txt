https://www.vietjetair.com/Sites/Web/vi-VN/NewsDetail/quang-cao-cung-vietjet/%203999/quang-cao-cung-vietjet
QUẢNG CÁO CÙNG VIETJET

VietJet là Hãng hàng không thế hệ mới với những ưu điểm vượt trội về chất lượng dịch vụ, hiệu quả hoạt động cùng với đội tàu bay mới và hiện đại.

Chương trình quảng bá thương hiệu trên máy bay VietJet trở thành một hiện tượng trong ngành quảng cáo tại Việt Nam. Đây chính là cơ hội tốt để chúng tôi có thể cùng với Quý đối tác phát triển giá trị thương hiệu không chỉ trong nước mà còn vươn tới các nước khác trên thế giới.
 

Thông tin liên hệ quảng cáo:
Ms. Nguyễn Thị Thanh Dung – Trưởng Ban Kinh doanh Quảng cáo
Email: dungnguyentt@vietjetair.com
Điện thoại: 09359.11111
