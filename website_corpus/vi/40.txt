https://www.vietjetair.com/Sites/Web/vi-VN/NewsDetail/khach-hang-doanh-nghiep/%202666/khach-hang-doanh-nghiep
Khách hàng doanh nghiệp

5 lý do bạn nên tham gia chương trình SKY CORPORATE

Chi phí hiệu quả nhất
- Nhiều hạng vé và mức giá để lựa chọn;
- Nâng cấp lên hạng vé Đặc biệt linh hoạt nhằm tiết kiệm nhiều chi phí đi lại cho Công ty;
- Giá vé rẻ với tính linh hoạt cao như thay đổi chuyến bay trước 03h so với giờ khởi hành; mua thêm các dịch vụ như: hành lý ký gửi, suất ăn, hàng lưu niệm…..

Dễ dàng quản lý
- Tổ chức/ Doanh nghiệp/ Công ty được cấp tài khoản và tên đăng nhập để chủ động đặt xuất vé và thay đổi chuyến bay;
- Hệ thống đặt vé thân thiện, dễ thao tác;
- Theo dõi, kiểm tra, quản lý lịch công tác của cán bộ nhân viên và quản lý ngân sách đi lại của Công ty với cơ sở báo cáo đầy đủ, toàn diện;

Lịch bay linh hoạt
- Mạng đường bay rộng khắp: hơn 60 đường bay trong nước và quốc tế; 300 chuyến bay/ ngày;
- Thuận tiện thay đổi, lựa chọn chuyến bay.

Ưu đãi nhiều dịch vụ
- Kênh hotline phục vụ 24/7

Điểm thưởng tặng vé máy bay
- Tặng thưởng vé máy bay miễn cước trong nước và quốc tế dựa trên doanh số mua vé theo từng quý tổng kết.
Gói dịch vụ của chúng tôi giúp Công ty của bạn giảm thiểu thật nhiều chi phí đi lại, quản lý báo cáo dễ dàng và đồng thời nhận được nhiều quà tặng.

HÃY ĐĂNG KÝ CHƯƠNG TRÌNH SKY CORPORATE NGAY BÂY GIỜ
