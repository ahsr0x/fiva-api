https://www.vietjetair.com/Sites/Web/vi-VN/NewsDetail/vietjet-flight-care/%204466/chuong-trinh-bao-hiem-flight-care
Chương trình bảo hiểm Flight Care

Vietjet hân hạnh tặng Quý khách chương trình bảo hiểm Flight Care khi mua vé hạng Deluxe hoặc SkyBoss với nhiều quyền lợi bảo hiểm dành cho chuyến bay cùng sự hỗ trợ tốt nhất, mang lại sự yên tâm, vui vẻ cho Quý khách khi bay cùng Vietjet!

Quý khách vui lòng tham khảo thông tin như sau:


Phần 1 : Thông tin chung chương trình bảo hiểm Flight Care

1.1 Thời hạn bảo hiểm

-Thời hạn bảo hiểm bắt đầu kể từ thời điểm Chuyến Bay dự kiến cất cánh, theo lịch bay được công bố cập nhật vào 22h00 (theo giờ địa phương) của ngày liền kề trước ngày khai thác Chuyến Bay đó.

- Hiệu lực bảo hiểm kết thúc kể từ thời điểm Chuyến Bay hạ cánh và xảy ra các sự kiện bảo hiểm như Trễ Chuyến Bay, Chuyến Bay Bị Hủy, Chuyến Bay Quay Đầu, Chuyến Bay Đáp Xuống Sân Bay Khác, Lỡ Chuyến Bay kế tiếp hoặc Chuyến Bay hạ cánh không phát sinh sự kiện bảo hiểm


1.2 Đối tượng tham gia bảo hiểm (Người được bảo hiểm)

Áp dụng cho tất cả hành khách sử dụng loại giá vé Deluxe/SkyBoss của Vietjet có độ tuổi từ 02 (hai) tuổi trở lên, không phân biệt quốc tịch, đáp ứng điều kiện đi lại theo Điều lệ vận chuyển của Vietjet, khi sử dụng trên các chuyến bay thường lệ (nội địa và quốc tế) do Vietjet khai thác trong Thời Hạn Bảo Hiểm.

Flight Care là chương trình bảo hiểm do Vietjet mua và tặng cho hành khách. Đối với 01 (Một) vé máy bay thuộc loại giá vé Deluxe/Skyboss, hành khách được hưởng quyền lợi bảo hiểm của Deluxe Flight Care/SkyBoss Flight Care tương ứng khi phát sinh Sự kiện Bảo hiểm.


1.3 Sự kiện bảo hiểm

Quyền lợi bảo hiểm của Quý khách được hưởng theo các sự kiện bảo hiểm như sau:

- Trễ Chuyến Bay (Chuyến Bay bị chậm) từ 180 phút trở lên

- Chuyến Bay bị Hủy

- Chuyến Bay Quay Đầu

- Chuyến Bay Đáp Xuống Sân Bay Khác

- Lỡ Chuyến Bay Kế Tiếp (chỉ dành cho hành khách SkyBoss)

- Hành Lý Bị Mất/Thất Lạc

- Hành Lý Đến Chậm.

Chi tiết về định nghĩa Sự kiện Bảo hiểm, vui lòng truy cập thêm tại trang thông tin điện tử: vietjetair.tracuubaohiem.vn, phần "Thông tin chương trình bảo hiểm Flight Care - Quyền lợi bảo hiểm".


1.4 Điểm loại trừ

- Không áp dụng cho các chuyến bay thuê chuyến của Vietjet và chuyến bay do Thái Vietjet khai thác.

- Không áp dụng cho hành khách sử dụng loại giá vé Eco, vé thưởng/tặng…

- Không áp dụng cho em bé dưới 02 tuổi.

- Người được bảo hiểm bị Hãng Hàng Không từ chối vận chuyển.

- Người được bảo hiểm có hành vi uy hiếp an ninh và an toàn bay.

- Chuyến Bay không được thực hiện hoặc Chuyến Bay Bị Trễ/Hủy/Quay Đầu do quyết định của cơ quan quản lý Nhà nước.

- Chuyến Bay không thể thực hiện do sân bay đóng cửa, bị giới nghiêm hoặc do các lệnh cấm của cơ quan quản lý Nhà nước.

- Chiến tranh, khủng bố, đình công/bãi công, hoạt động quân sự, máy bay bị tấn công.

- Các Chuyến Bay bị ảnh hưởng trực tiếp bởi các dịch bệnh theo thông báo chính thức của Bộ y tế hoặc WHO (*).

(*) Loại trừ này áp dụng cho các Chuyến Bay xảy ra các Sự kiện bảo hiểm (Chuyến Bay Bị Hủy, Chuyến Bay Quay Đầu, Chuyến Bay Đáp Xuống Sân Bay Khác) do:

Hành Khách hoặc Phi hành đoàn bị nhiễm dịch bệnh
Sân Bay hạ cánh/cất cánh bị cách ly hoặc hạ cánh/cất cánh do dịch bệnh.
Yêu cầu của Cơ quan có thẩm quyền (Trường hợp các cơ quan chức năng gửi văn bản yêu cầu dừng các Chuyến bay)
Các điều kiện loại trừ khác, Quý khách vui lòng xem thêm chi tiết tại đây.


Phần 2 : Tóm tắt quyền lợi bảo hiểm và số tiền bảo hiểm


QUYỀN LỢI BẢO HIỂM (*)  HẠN MỨC SỐ TIỀN BẢO HIỂM (VND)
DELUXE FLIGHT CARE  SKYBOSS FLIGHT CARE
2.1 Chuyến Bay Bị Chậm (Trễ Chuyến Bay)
Từ 180 phút đến dưới 360 phút   300.000 400.000
Từ 360 phút trở lên 500.000 600.000
2.2 Chuyến Bay Quay Đầu 800.000 1.000.000
2.3 Chuyến Bay Đáp Xuống Sân Bay Khác   800.000 1.000.000
2.4 Hủy Chuyến (không có chuyến bay thay thế hoặc chuyển sang chuyến bay khác không mong muốn.  800.000 1.000.000
2.5 Lỡ Chuyến Bay Kế Tiếp (bao gồm cả trên hãng hàng không khác Không áp dụng   1.000.000
(tối đa 200.000 cho mỗi 3 giờ)
2.6 Mất Hành Lý, Thất lạc   5.000.000
(tối đa 2.500.000/kiện hành lý) 10.000.000
(tối đa 5.000.000/kiện hành lý)
2.7 Hành Lý Đến Chậm    1.500.000
(tối đa 500.000 cho mỗi 06 giờ đến chậm)    2.000.000
(tối đa 1.000.000 cho mỗi 06 giờ đến chậm)
 

(*) Chi tiết về quyền lợi bảo hiểm, xin vui lòng xem thêm tại đây


Phần 3 : Thủ tục bồi thường

3.1 Thời hạn thông báo và nộp hồ sơ bồi thường: trong vòng 365 ngày kể từ ngày xảy ra Sự Kiện Bảo Hiểm


3.2 Thời hạn giải quyết bồi thường: Thời hạn giải quyết bồi thường là 10 Ngày làm việc kể từ ngày nhận hồ sơ yêu cầu bồi thường đầy đủ, hợp lệ (được ghi nhận theo dấu bưu điện).
 

3.3 Phương thức thực hiện bồi thường :

- Thực hiện cuộc gọi tới số điện thoại hotline: (+84)1900 638388

- Truy cập trang thông tin điện tử và làm theo hướng dẫn: vietjetair.tracuubaohiem.vn

- Gửi yêu cầu bồi thường qua địa chỉ email: boithuongvietjet@pti.com.vn.


3.4 Đơn vị giải quyết bồi thường

Công ty Bảo hiểm Bưu Điện Thời Đại Số

Địa chỉ: Tầng 3, Tòa nhà Comatece, số 61, Ngụy Như Kon Tum, P. Nhân Chính, Q. Thanh Xuân, Thành Phố Hà Nội, Việt Nam.

Hotline :(+84)1900 6383 88

Email: chamsockhachhang@pti.com.vn